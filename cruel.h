/*
 * Cruel - header-only wrapper around cURL to make simple requests in C++.
 * Copyright (C) 2021 Volk_Milit (aka Ja'Virr-Dar)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CRUEL_H
#define CRUEL_H

#include <iostream>
#include <vector>
#include <curl/curl.h>
#include "third-party/json/single_include/nlohmann/json.hpp"

using namespace std;
using namespace nlohmann;

/**
 * GET, POST, DEL, PUT methods used in request().
 *
 * @brief The Methods enum
 */
enum Methods
{
    GET = 0,
    POST,
    DEL,
    PUT
};

/**
 * Contains std::string_view, can return string_view (to_string method) or json (to_json method).
 *
 * For json referense, see nlohmann/json.
 *
 * @brief The Response struct
 */
struct Response
{
public:
    Response(string_view data) :
        m_data(data){}

    string_view to_string() const
    {
        return m_data;
    }

    json to_json() const
    {
        return json::parse(m_data);
    }

private:
    string_view m_data;
};

class Cruel
{
public:
    Cruel(string url) :
        m_url(move(url))
    {
        m_curlptr = curl_easy_init();
        init_curl();
    }

    ~Cruel()
    {
        if (m_curlptr)
        {
            if (m_headers)
                curl_slist_free_all(m_headers);

            curl_easy_cleanup(m_curlptr);
        }
    }

    /**
     * Add custom header to your request.
     *
     * @brief add_header
     * @param header
     */
    void add_header(const pair<string, string> &header)
    {
        const string make_header = header.first + ":" + header.second;
        m_headers = curl_slist_append(m_headers, make_header.c_str());
    }

    /**
     * Set custom SSL sertificate.
     *
     * @brief set_certificate
     * @param capath
     */
    void set_certificate(const char *capath)
    {
        curl_easy_setopt(m_curlptr, CURLOPT_CAPATH, capath);
    }

    /**
     * Set SSL enabled/disabled.
     *
     * Note: SSL is enabled by default.
     *
     * @brief set_ssl
     * @param disabled
     */
    void set_ssl(bool disabled)
    {
        m_no_ssl = disabled;
    }

    /**
     * Make request using enum.
     *
     * Available requests: GET, POST, DEL, PUT.
     *
     * @brief request
     * @param method
     * @param data
     */
    void request(Methods method, const string &data)
    {
        switch(method) {
        case GET:
            get();
            break;
        case POST:
            post(data);
            break;
        case DEL:
            del(data);
            break;
        case PUT:
            put(data);
            break;
        default:
            assert("Unknown request type!");
            return;
        }
    }

    /**
     * Make request using enum.
     *
     * Available requests: GET, POST, DEL, PUT.
     *
     * @brief request
     * @param method
     * @param data
     */
    void request(Methods method, const vector<pair<string, string>> &data)
    {
        switch(method) {
        case GET:
            get();
            break;
        case POST:
            post(data);
            break;
        case DEL:
            del(data);
            break;
        case PUT:
            put(data);
            break;
        default:
            assert("Unknown request type!");
            return;
        }
    }

    /**
     * Make request using enum.
     *
     * Available requests: GET, POST, DEL, PUT.
     *
     * @brief request
     * @param method
     * @param data
     */
    void request(Methods method, const json &data)
    {
        switch(method) {
        case GET:
            get();
            break;
        case POST:
            post(data);
            break;
        case DEL:
            del(data);
            break;
        case PUT:
            put(data);
            break;
        default:
            assert("Unknown request type!");
            return;
        }
    }

    /**
     * Make GET request.
     *
     * @brief get
     */
    void get()
    {
        curl_easy_setopt(m_curlptr, CURLOPT_FOLLOWLOCATION, 1L);
        make_request();
    }

    void post(const string &data)
    {
        curl_easy_setopt(m_curlptr, CURLOPT_POSTFIELDS, data.c_str());
        curl_easy_setopt(m_curlptr, CURLOPT_POSTFIELDSIZE, static_cast<long>(data.length()));

        make_request();
    }

    /**
     * Make POST request using key-value pair.
     *
     * @brief post
     * @param data
     */
    void post(const vector<pair<string, string>> &data)
    {
        string request;

        for (const auto &item : data)
        {
            request.append('&' + item.first + '=' + item.second);
            cout << item.first;
        }

        curl_easy_setopt(m_curlptr, CURLOPT_POSTFIELDS, request.c_str());
        curl_easy_setopt(m_curlptr, CURLOPT_POSTFIELDSIZE, static_cast<long>(request.length()));

        make_request();
    }

    /**
     * Make POST request using json.
     *
     * @brief post
     * @param data
     */
    void post(const json &data)
    {
        const string request = data.dump();

        curl_easy_setopt(m_curlptr, CURLOPT_POSTFIELDS, request.c_str());
        curl_easy_setopt(m_curlptr, CURLOPT_POSTFIELDSIZE, static_cast<long>(request.length()));

        m_headers = curl_slist_append(m_headers, "Content-Type: application/json");
        m_headers = curl_slist_append(m_headers, "charsets: utf-8");

        if (!m_headers)
        {
            m_error = "Can't setup headers.";
            return;
        }

        make_request();
    }

    /**
     * Send mime form request.
     *
     * files param is optional.
     *
     * @brief send_form
     * @param data
     * @param files
     */
    void send_form(
            const vector<pair<string, string>> &data,
            const vector<pair<string, string>> &files = {})
    {
        curl_mime *form = curl_mime_init(m_curlptr);
        curl_mimepart *field = nullptr;

        // According to documentation, this field is mandatory
        // for some services, so keep it as is
        field = curl_mime_addpart(form);
        curl_mime_name(field, "submit");
        curl_mime_data(field, "send", CURL_ZERO_TERMINATED);

        for (const auto &item : data)
        {
            field = curl_mime_addpart(form);
            curl_mime_name(field, item.first.data());
            curl_mime_data(field, item.second.c_str(), CURL_ZERO_TERMINATED);
        }

        for (const auto &file : files)
        {
            field = curl_mime_addpart(form);
            curl_mime_name(field, file.first.data());
            curl_mime_filedata(field, file.second.c_str());
        }

        curl_easy_setopt(m_curlptr, CURLOPT_MIMEPOST, form);

        make_request();

        curl_mime_free(form);
    }

    /**
     * Make DELETE request using string value.
     *
     * @brief del
     * @param data
     */
    void del(const string &data)
    {
        curl_easy_setopt(m_curlptr, CURLOPT_CUSTOMREQUEST, "DELETE");
        post(data);
    }

    /**
     * Make DELETE request using key-value pair.
     *
     * @brief del
     * @param data
     */
    void del(const vector<pair<string, string>> &data)
    {
        curl_easy_setopt(m_curlptr, CURLOPT_CUSTOMREQUEST, "DELETE");
        post(data);
    }

    /**
     * Make DELETE request using json.
     *
     * @brief del
     * @param data
     */
    void del(const json &data)
    {
        curl_easy_setopt(m_curlptr, CURLOPT_CUSTOMREQUEST, "DELETE");
        post(data);
    }

    /**
     * Make PUT request using string value.
     *
     * @brief put
     * @param data
     */
    void put(const string &data)
    {
        curl_easy_setopt(m_curlptr, CURLOPT_CUSTOMREQUEST, "PUT");
        post(data);
    }

    /**
     * Make PUT request key-value pair.
     *
     * @brief put
     * @param data
     */
    void put(const vector<pair<string, string>> &data)
    {
        curl_easy_setopt(m_curlptr, CURLOPT_CUSTOMREQUEST, "PUT");
        post(data);
    }

    /**
     * Make PUT request using json.
     *
     * @brief put
     * @param data
     */
    void put(const json &data)
    {
        curl_easy_setopt(m_curlptr, CURLOPT_CUSTOMREQUEST, "PUT");
        post(data);
    }

    /**
     * Get response from server.
     *
     * @brief response
     * @return Response
     */
    Response response() const
    {
        return Response(m_response);
    }

    /**
     * Check, if there was an error during request.
     *
     * @brief has_error
     * @return boolean
     */
    bool has_error() const
    {
        return !m_error.empty();
    }

    /**
     * Get error string.
     *
     * @brief error
     * @return string_view
     */
    string_view error() const
    {
        return m_error;
    }

    /**
     * Get status code.
     *
     * @brief status
     * @return long
     */
    long status() const
    {
        return m_status_code;
    }

private:
    static size_t write_string_callback(char *contents, size_t size, size_t nmemb, string *str)
    {
        str->append(contents, size * nmemb);
        return size * nmemb;
    }

    void make_request()
    {
        m_response.clear();

        CURLcode res;

        // Setup headers if necessary
        if (m_headers)
        {
            res = curl_easy_setopt(m_curlptr, CURLOPT_HTTPHEADER, m_headers);

            if(res != CURLE_OK)
            {
                m_error = curl_easy_strerror(res);
                return;
            }
        }

        curl_easy_setopt(m_curlptr, CURLOPT_WRITEFUNCTION, write_string_callback);
        curl_easy_setopt(m_curlptr, CURLOPT_WRITEDATA, &m_response);

        res = curl_easy_perform(m_curlptr);
        if(res != CURLE_OK)
            m_error = curl_easy_strerror(res);

        curl_easy_getinfo(m_curlptr, CURLINFO_RESPONSE_CODE, &m_status_code);
    }

    void init_curl()
    {
        if (!m_curlptr)
        {
            m_error = "Failed to inizialize CURL!";
            return;
        }

        // Use SSL by default
        if (!m_no_ssl)
        {
            curl_easy_setopt(m_curlptr, CURLOPT_SSL_VERIFYPEER, 0L);
            curl_easy_setopt(m_curlptr, CURLOPT_SSL_VERIFYHOST, 0L);
        }

        curl_easy_setopt(m_curlptr, CURLOPT_URL, m_url.data());
    }

    string m_url;
    string m_response;
    string_view m_error;
    long m_status_code = 0;

    bool m_no_ssl = false;

    CURL *m_curlptr = nullptr;
    struct curl_slist *m_headers = nullptr;
};

#endif // CRUEL_H
