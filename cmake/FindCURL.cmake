FIND_PATH(CURL_INCLUDES curl.h
    HINTS
    $ENV{CURL_ROOT}
    PATH_SUFFIXES include/curl include/local include

    PATHS
    ENV CPATH
    /usr/
    /usr/local/
    /opt/local/
    ${CURL_ROOT}/
)

FIND_LIBRARY(CURL_LIBRARY NAMES curl
    NAMES curl
    HINTS
    $ENV{CURL_ROOT}
    PATH_SUFFIXES lib lib64 local/lib local/lib64

    PATHS
    ENV LD_LIBRARY_PATH
    ENV LIBRARY_PATH
    /usr/
    ${CURL_ROOT}/
)

if(CURL_LIBRARY)
    set(CURL_LIBRARIES "${CURL_LIBRARY}" CACHE STRING "CURL Libraries")
endif()

if(CURL_INCLUDES AND CURL_LIBRARY)
    set(CURL_FOUND TRUE)
endif(CURL_INCLUDES AND CURL_LIBRARY)
