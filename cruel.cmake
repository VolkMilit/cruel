add_library(project INTERFACE)

find_package(CURL REQUIRED)

if(NOT CURL_FOUND)
    message(FATAL_ERROR "CUrl not found!")
endif()

include_directories("third-party/json/single_include")
include_directories(${CURL_INCLUDE_DIR}) 
