# Cruel

Header-only wrapper around cURL to make simple requests in C++.

*Pun intended.*

### Motivation

I started this library, because Python is too slow and I'm disappointed in Rust syntax.

All I wanted is simple header-only library to make requests, with minimum effort.

### cURL installation

Linux: depending on your distribution install either `curl` or `libcurl-dev` packages.

Windows: download libcurl from [here](https://curl.se/download.html) or using vcpkg.

### Usage

Clone cruel repository to your project directory:

```sh
git clone --recursive --depth 1 https://gitlab.com/VolkMilit/cruel.git
```

Include Cruel to your cmake project:

```cmake
include(cruel/cruel.cmake)
# Other code
target_link_libraries(${PROJECT_NAME} ${CURL_LIBRARY})
```

*Don't forget to add cruel/cruel.h to your project source files!*

**Note:** on Windows you need also add paths to curl, if you're not using vcpkg:

```sh
-DCURL_LIBRARY=/path/to/curl/lib/libcurl.dll.a # it MUST be dll.a
-DCURL_INCLUDE_DIR=/path/to/curl/includes
```

### Examples

GET request example:

```cpp
#include "cruel/cruel.h"

#include <iostream>

using namespace std;

int main() {
	Cruel cruel("https://google.com");
	
	if (!cruel.has_error()) {
		cruel.get();
		
		if (cruel.has_error())
			cout << cruel.error() << "\n";
		else
			cout << cruel.reponse().to_string() << "\n";
	}
	
	return 0;
}
```

Example using [Telegram bot api](https://api.telegram.org):

```cpp
#include "cruel/cruel.h"

#include <iostream>
#include <vector>

using namespace std;

int main() {
	// By key-value
	// Will translate into &char_id=-100123456789&text="Test message"
	static const vector<pair<string, string>> data = {
		{"chat_id", "-100123456789"},
		{"text", "Test message"}
	};
	
	// By json
	json j;
	j["chat_id"] = "-100123456789";
	j["text"] = "Test message";

	static const string token = "bot_token";
	Cruel cruel("https://api.telegram.org/bot" + token + "/sendMessage");
	
	if (!cruel.has_error()) {
		cruel.post(data);
		
		if (cruel.has_error())
			cout << cruel.error() << "\n";
		else
			cout << cruel.reponse().to_json() << "\n";
	}
	
	return 0;
}
```

### License

```
Cruel - header-only wrapper around cURL to make simple requests in C++.
Copyright (C) 2021 Volk_Milit (aka Ja'Virr-Dar)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
